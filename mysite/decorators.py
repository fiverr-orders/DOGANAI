def has_trial_or_subscription(user):
    return user.has_active_trial or user.has_active_subscription
