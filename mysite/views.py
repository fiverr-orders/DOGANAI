import json
import os
from wsgiref.util import FileWrapper

import pyexcel
from botocore.exceptions import ClientError
from django.contrib import messages
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.views import auth_logout
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail
from django.shortcuts import render, HttpResponse, redirect, HttpResponseRedirect
from django.template import loader
from pandas.errors import EmptyDataError

from mysite.buckets import get_or_create_user_bucket, get_user_bucket_contents, upload_user_file, read_file
from mysite.decorators import has_trial_or_subscription
from mysite.models import CustomUser

godhand = ""


# Create your views here.
def index(request):
    data = {}

    # Contact Page
    if request.method == 'POST':
        data['name'] = request.POST.get('name')
        data['email'] = request.POST.get('email')
        data['phone'] = request.POST.get('phone')
        data['msg_subject'] = request.POST.get('msg_subject')
        data['message'] = request.POST.get('message')

        # email = EmailMessage("Dogan.AI - Leads", data, to=['help@foundationai.org'])

        message = "Name: " + data['name'] + "\n" + "Email: " + data['email'] + "\n" + "Phone: " + data[
            'phone'] + "\n" + "Subject: " + data['msg_subject'] + "\n" + "Message: " + data['message']

        send_mail('DOGAN.AI - CONTACT - ' + data['name'], message, 'help@foundationai.org',
                  ['help@foundationai.org', 'john@dogan.ai'], fail_silently=False)

        messages.success(request, "Your information has been sent! We will contact you shortly.")

        return HttpResponseRedirect("/#contact")

    return render(request, "core/index.html")


def download_white_paper(request):
    template_path = os.getcwd() + "/static/pdf/dogan.pdf"
    # wrapper = FileWrapper(open(template_path))
    wrapper = FileWrapper(open(template_path, 'rb'))

    response = HttpResponse(wrapper, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=Dogan Technologies - Information.pdf'
    response['Content-Length'] = os.path.getsize(template_path)
    return response


def login(request):
    if request.method != 'POST':
        return render(request, 'core/login.html')

    username = request.POST.get('username')
    password = request.POST.get('password')

    user = authenticate(username=username, password=password)

    if user is not None and user.is_active:
        auth_login(request, user)
        get_or_create_user_bucket(request.user.username)

        return redirect(dashboard)
    else:
        test2 = "Please!!"
        test3 = "Username or Password invalid!!!"

        return render(request, 'core/login.html', {'test3': test3, 'test2': test2})


def register(request):
    data = {}

    if request.method == 'POST':
        data['username'] = request.POST.get('username')
        data['email'] = request.POST.get('email')
        data['password'] = request.POST.get('password')
        data['password2'] = request.POST.get('password2')
        email = CustomUser.objects.filter(email=data['email'])
        user = CustomUser.objects.filter(username=data['username'])
        if email.exists() and user.exists() or email.exists() or user.exists():
            test5 = "Email and User id already exists"

            return render(request, 'core/register.html', {'test5': test5})

        elif data['password'] != data['password2']:
            test5 = "Password mismatch!"

            return render(request, 'core/register.html', {'test5': test5})

        else:
            try:
                user = CustomUser.objects.create_user(username=data['username'],
                                                      email=data['email'],
                                                      password=data['password'])

                notificaton = "Thank you for registering ! Your account has now been created and you can log in by using your email address and password "
                return redirect(login)

            except Exception as e:
                print(str(e))
                return render(request, 'core/login.html')

    return render(request, 'core/register.html')


@login_required
@user_passes_test(has_trial_or_subscription, login_url='/payments/subscriptions/')
def dashboard(request):
    if request.method == 'POST' and request.FILES['csv_file']:
        csv_file = request.FILES['csv_file']
        upload_user_file(request.user.username, csv_file)

    files = get_user_bucket_contents(request.user.username)
    return render(request, 'core/dashboard.html', {'files': files})


@login_required
@user_passes_test(has_trial_or_subscription, login_url='/payments/subscriptions/')
def create_file(request, response):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        t = loader.get_template('template.txt')
        c = {'data': myfile}
        response.write(t.render(c))
        return response

    return render(request, 'core/dashboard.html')


@login_required
def table_viewer(request):
    p.save_as(file_name=godhand, dest_file_name='core/open.handsontable.html')
    return render(request, 'core/open.handsontable.html')


@login_required
@user_passes_test(has_trial_or_subscription, login_url='/payments/subscriptions/')
def analysis(request):
    from django.conf import settings
    host_url = f"{settings.DOGANAI_URL}/jupyter/"
    current_user = request.user.username
    return render(request, 'core/analysis.html', locals())


@login_required
@user_passes_test(has_trial_or_subscription, login_url='/payments/subscriptions/')
def settings(request):
    return render(request, 'core/settings.html')


@login_required
def profile(request):
    return render(request, 'core/profile.html')


@login_required
def logout(request):
    auth_logout(request)
    return redirect(index)


def download_ai_paper(request):
    template_path = os.getcwd() + "/static/pdf/solutions.pdf"
    # wrapper = FileWrapper(open(template_path))
    wrapper = FileWrapper(open(template_path, 'rb'))

    response = HttpResponse(wrapper, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=Dogan Technologies - Solutions.pdf'
    response['Content-Length'] = os.path.getsize(template_path)
    return response


@login_required
@user_passes_test(has_trial_or_subscription, login_url='/payments/subscriptions/')
def grid_view(request):
    file_name = request.GET.get('file_name')

    if '.csv' not in file_name:
        redirect('dashboard')

    columns = []
    try:
        file_data = read_file(request.user.username, file_name=file_name)
    except EmptyDataError:
        file_data = {}
    except ClientError as ex:
        if ex.response['Error']['Code'] == 'NoSuchKey':
            file_data = {}
        else:
            raise

    return render(request, 'core/grid-view.html', locals())
