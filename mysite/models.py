from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone


class CustomUser(AbstractUser):
    free_trial_expiry_time = models.DateTimeField(blank=True, null=True)
    premium_package_expiry_time = models.DateTimeField(blank=True, null=True)

    @property
    def free_trial_status(self):
        current_dt = timezone.now()
        if self.free_trial_expiry_time:
            if self.free_trial_expiry_time < current_dt:
                return "EXPIRED"
            else:
                diff = self.free_trial_expiry_time - current_dt
                return "ACTIVE - " + self.get_time_str(diff)
        return "START TRIAL"

    @property
    def has_active_trial(self):
        current_dt = timezone.now()
        if self.free_trial_expiry_time:
            if self.free_trial_expiry_time < current_dt:
                return False
            else:
                return True
        return False

    @property
    def has_active_subscription(self):
        current_dt = timezone.now()
        if self.premium_package_expiry_time:
            if self.premium_package_expiry_time < current_dt:
                return False
            else:
                return True
        return False

    @staticmethod
    def get_time_str(diff):
        total_secs = diff.total_seconds()
        # days = 0
        # hours = 0
        # minutes = 0
        # seconds = 0
        time_str = ""
        if total_secs > 86400:
            days = int(total_secs / 86400)
            total_secs -= (days * 86400)
            if days > 0:
                time_str += str(days) + "D:"
        if total_secs > 3600:
            hours = int(total_secs / 3600)
            total_secs -= (hours * 3600)
            if hours > 0:
                time_str += str(hours) + "H:"
        if total_secs > 60:
            minutes = int(total_secs / 60)
            total_secs -= (minutes * 60)
            if minutes > 0:
                time_str += str(minutes) + "M:"
        # if total_secs > 0:
        #     seconds = int(total_secs)
        #     if seconds > 0:
        #         time_str += str(seconds) + "S:"
        return time_str.strip(":") + " Left"
