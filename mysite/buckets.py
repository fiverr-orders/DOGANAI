import json

import boto3
import pandas as pd
import io
from botocore.exceptions import ClientError
from django.core.files.storage import FileSystemStorage

from django.conf import settings


def get_user_bucket_contents(username):
    s3 = boto3.client('s3')
    bucket = f'{settings.AWS_BUCKET_PREFIX}-{username}'

    try:
        contents = [key['Key'].lstrip('data') for key in s3.list_objects(Bucket=bucket, Prefix='data/')['Contents']]
    except KeyError:
        contents = list()

    filtered_contents = [
        x.lstrip('/') for x in contents if x not in ['/', '/.ipynb_checkpoints/'] and 'checkpoint' not in x
    ]

    return [x for x in filtered_contents if not x.endswith('/')]


def get_or_create_user_bucket(username):
    s3 = boto3.client('s3')
    bucket = f'{settings.AWS_BUCKET_PREFIX}-{username}'
    location = {'LocationConstraint': settings.AWS_DEFAULT_REGION}

    try:
        return s3.head_bucket(Bucket=bucket)
    except ClientError:
        return s3.create_bucket(Bucket=bucket, CreateBucketConfiguration=location)


def upload_user_file(username, file):
    s3 = boto3.client('s3')
    bucket = f'{settings.AWS_BUCKET_PREFIX}-{username}'
    fs = FileSystemStorage()
    filename = fs.save(file.name, file)

    return s3.upload_file(f'.{fs.url(filename)}', bucket, f'data/{filename}')


def read_file(username, file_name):
    s3 = boto3.client('s3')
    bucket = f'{settings.AWS_BUCKET_PREFIX}-{username}'

    s3_file = s3.get_object(Bucket=bucket, Key=f'data/{file_name}')
    df = pd.read_csv(io.BytesIO(s3_file['Body'].read()))

    json_data = df.to_json(orient='records')
    dict_data = json.loads(json_data)
    return dict_data
