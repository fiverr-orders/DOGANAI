from datetime import datetime

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from paypal.standard.forms import PayPalPaymentsForm

from payments.models import Invoice, ProductPlan


@csrf_exempt
@login_required()
def payment_done(request, invoice_id):
    invoice = Invoice.objects.get(pk=invoice_id)
    invoice.paid_at = datetime.now()
    invoice.paid = True

    invoice.save()

    request.user.premium_package_expiry_time = timezone.now() + timezone.timedelta(days=30)
    request.user.save()

    return render(request, 'payments/payment_done.html')


@csrf_exempt
def payment_canceled(request):
    return render(request, 'payments/payment_cancelled.html')


def index(request):
    return render(request, 'payments/index.html')


def show_product(request):
    return redirect(reverse('index'))


@login_required
def start_trial(request):
    context = {}
    if request.user.free_trial_status == "START TRIAL":
        request.user.free_trial_expiry_time = timezone.now() + timezone.timedelta(days=7)
        request.user.save()
        context['message'] = 'You trial has started'

    return render(request, 'payments/index.html', context)


@login_required
def subscriptions(request):
    plans = ProductPlan.objects.all()
    has_subscription = Invoice.objects.filter(user=request.user).count() > 0
    context = {
        "trial_text": request.user.free_trial_status,
        "has_subscription": has_subscription,
        "plans": plans
    }

    return render(request, 'payments/subscription.html', context)


@login_required()
def subscribe(request, plan_id):
    plan = ProductPlan.objects.get(pk=plan_id)
    host = request.get_host()
    price = plan.price
    billing_cycle = plan.billingCycle.frequency_count
    billing_cycle_unit = plan.billingCycle.frequency_unit

    total_invoices = Invoice.objects.all().filter(user__username__exact=request.user.username).count()
    invoice_no = f"INV{request.user.id}-{str(total_invoices + 1)}"

    invoice = Invoice.objects.create(
        productPlan=plan,
        user=request.user,
        invoice_no=invoice_no,
        amount=price
    )

    paypal_dict = {
        "cmd": "_xclick-subscriptions",
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        "a3": price,  # monthly price
        "p3": billing_cycle,  # duration of each unit (depends on unit)
        "t3": billing_cycle_unit,  # duration unit ("M for Month")
        "src": "1",  # make payments recur
        "sra": "1",  # reattempt payment on payment error
        "no_note": "1",  # remove extra notes (optional)
        'item_name': plan.name,
        # 'custom': 1,  # custom data, pass something meaningful here
        'invoice': invoice_no,
        'currency_code': 'USD',
        'notify_url': f'http://{host}{reverse("paypal-ipn")}',
        'return_url': f'http://{host}{reverse("payment-done", kwargs={"invoice_id": invoice.id})}',
        'cancel_return': f'http://{host}{reverse("payment-cancelled")}',
    }

    form = PayPalPaymentsForm(initial=paypal_dict, button_type="subscribe")
    return render(request, 'payments/subscribe.html', locals())


@login_required
def history(request):
    invoices = Invoice.objects.filter(user=request.user, paid=True)

    return render(request, 'payments/history.html', locals())
