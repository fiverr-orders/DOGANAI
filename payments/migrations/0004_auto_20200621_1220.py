# Generated by Django 3.0.7 on 2020-06-21 12:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0003_auto_20200621_1204'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productplanfeature',
            name='productPlan',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='features', to='payments.ProductPlan'),
        ),
    ]
