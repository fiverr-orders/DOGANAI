# Generated by Django 3.0.7 on 2020-06-21 13:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0004_auto_20200621_1220'),
    ]

    operations = [
        migrations.AddField(
            model_name='billingcycle',
            name='frequency_count',
            field=models.IntegerField(default=0),
        ),
    ]
