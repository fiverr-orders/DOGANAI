from django.contrib import admin
from django.contrib.admin import ModelAdmin

from payments.models import Product, BillingCycle, ProductPlan, ProductPlanFeature, Invoice


@admin.register(Product)
class ProductAdmin(ModelAdmin):
    pass


@admin.register(BillingCycle)
class BillingCycleAdmin(ModelAdmin):
    pass


@admin.register(ProductPlan)
class ProductPlanAdmin(ModelAdmin):
    pass


@admin.register(ProductPlanFeature)
class ProductPlanFeatureAdmin(ModelAdmin):
    pass


@admin.register(Invoice)
class InvoiceAdmin(ModelAdmin):
    pass
