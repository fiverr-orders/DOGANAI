from django.urls import path

from payments import views

urlpatterns = [
    path('', views.index, name='index'),
    path('product/<int:product_id>/<slug:product_slug>/', views.show_product, name='show-product'),
    path('payment-done/<int:invoice_id>/', views.payment_done, name='payment-done'),
    path('payment-cancelled/', views.payment_canceled, name='payment-cancelled'),
    path('start_trial/', views.start_trial, name='start-trial'),
    path('subscriptions/', views.subscriptions, name='subscriptions'),
    path('subscribe/<int:plan_id>/', views.subscribe, name='subscribe'),
    path('history', views.history, name='history')
]
