from django.contrib.auth import get_user_model
from django.db import models


# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=191)
    type = models.CharField(max_length=8, default=None)
    category = models.CharField(max_length=8, default=None)
    slug = models.SlugField()
    description = models.TextField()
    image = models.ImageField(upload_to='products_images/', blank=True)

    def __str__(self):
        return self.name


class BillingCycle(models.Model):
    name = models.CharField(max_length=100)
    frequency_unit = models.CharField(max_length=10)
    frequency_count = models.IntegerField(default=0)
    tenure_type = models.CharField(max_length=10)
    total_cycles = models.IntegerField()

    def __str__(self):
        return "{}:{}".format(self.name, self.id)


class ProductPlan(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    name = models.CharField(max_length=50)
    description = models.TextField()
    billingCycle = models.ForeignKey(BillingCycle, on_delete=models.PROTECT, related_name="plans")
    price = models.DecimalField(max_digits=7, decimal_places=2, default=0.00)

    def __str__(self):
        return "{}:{}".format(self.name, self.id)


class ProductPlanFeature(models.Model):
    productPlan = models.ForeignKey(ProductPlan, on_delete=models.PROTECT, related_name="features")
    name = models.CharField(max_length=255)

    def __str__(self):
        return "{}:{}".format(self.name, self.id)


class Invoice(models.Model):
    productPlan = models.ForeignKey(ProductPlan, on_delete=models.PROTECT, null=True, blank=True, default=None)
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    invoice_no = models.CharField(blank=False, max_length=200, null=False)
    billed_at = models.DateTimeField(auto_now_add=True)
    paid_at = models.DateTimeField(null=True, blank=True)
    paid = models.BooleanField(default=False)
    amount = models.DecimalField(max_digits=7, decimal_places=2, default=0.0)

    def __str__(self):
        return "{}:{}".format(self.amount, self.user.username)
