from datetime import datetime

from django.contrib.auth import get_user_model
from django.core.mail import EmailMessage
from django.dispatch import receiver
from django.shortcuts import get_object_or_404
from paypal.standard.ipn.signals import valid_ipn_received, invalid_ipn_received
from paypal.standard.models import ST_PP_COMPLETED

from payments.models import Invoice


@receiver(valid_ipn_received)
def valid_payment_notification(sender, **kwargs):
    ipn_obj = sender

    # check for Buy Now IPN
    if ipn_obj.txn_type == 'web_accept':

        if ipn_obj.payment_status == ST_PP_COMPLETED:
            # payment was successful
            invoice = get_object_or_404(Invoice, id=ipn_obj.invoice)

            if invoice.amount == ipn_obj.mc_gross:
                invoice.paid = True
                invoice.paid_at = datetime.utcnow()
                invoice.save()

    # check for subscription signup IPN
    elif ipn_obj.txn_type == "subscr_signup":

        # get user id and activate the account
        id = ipn_obj.custom
        user = get_user_model().objects.get(id=id)
        user.active = True
        user.save()

        subject = 'Sign Up Complete'

        message = 'Thanks for signing up!'

        email = EmailMessage(subject,
                             message,
                             'admin@myshop.com',
                             [user.email])

        email.send()

    # check for subscription payment IPN
    elif ipn_obj.txn_type == "subscr_payment":

        # get user id and extend the subscription
        id = ipn_obj.custom
        user = get_user_model().objects.get(id=id)
        # user.extend()  # extend the subscription

        subject = 'Your Invoice for {} is available'.format(
            datetime.strftime(datetime.now(), "%b %Y"))

        message = 'Thanks for using our service. The balance was automatically ' \
                  'charged to your credit card.'

        email = EmailMessage(subject,
                             message,
                             'admin@myshop.com',
                             [user.email])

        email.send()

    # check for failed subscription payment IPN
    elif ipn_obj.txn_type == "subscr_failed":
        pass

    # check for subscription cancellation IPN
    elif ipn_obj.txn_type == "subscr_cancel":
        pass


@receiver(invalid_ipn_received)
def invalid_ipn_received(sender, **kwargs):
    print("invalid_ipn_received")
