"""doganai URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import json

from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.urls import path, include
from mysite import views as site_view
from django.conf import settings
from django.conf.urls.static import static


@login_required()
def user_data(request):
    user = request.user
    return HttpResponse(
        json.dumps({
            'username': user.username
        }),
        content_type='application/json'
    )


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', site_view.index, name='index'),
    path('download_white_paper', site_view.download_white_paper, name='download_white_paper'),
    path('login/', site_view.login, name='login'),
    path('register/', site_view.register, name='register'),
    path('dashboard/', site_view.dashboard, name='dashboard'),
    path('dashboard/grid-view/', site_view.grid_view, name="grid-view"),
    path('createFile/', site_view.create_file, name='createFile'),
    path('tableViewer/', site_view.table_viewer, name='tableViewer'),
    path('analysis/', site_view.analysis, name='analysis'),
    path('settings/', site_view.settings, name='settings'),
    path('profile/', site_view.profile, name='profile'),
    path('log_out/', site_view.logout, name="logout"),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('userdata', user_data, name='userdata'),
    path('paypal/', include('paypal.standard.ipn.urls')),
    path('payments/', include("payments.urls")),
    path('download_ai_paper', site_view.download_ai_paper, name='download_ai_paper'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
