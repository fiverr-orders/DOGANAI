from django.urls import path

from oauth.views import user_data

app_name = "oauth"
urlpatterns = [
    path('user_data', user_data, name='user_data')
]
