import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse


# Create your views here.
@login_required()
def user_data(request):
    user = request.user
    return HttpResponse(
        json.dumps({
            'username': user.username
        }),
        content_type='application/json'
    )
