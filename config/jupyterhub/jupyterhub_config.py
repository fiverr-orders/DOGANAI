import os

# This is how we tell Jupyter to use OAuth instead of the default
# authentication which is done using local Linux user accounts.
c.JupyterHub.authenticator_class = 'oauthenticator.generic.GenericOAuthenticator'

# Where should Django pass the authentication results back to?
c.GenericOAuthenticator.oauth_callback_url = os.environ['OAUTH2_CALLBACK_URL']

# What is the client ID and client secret for Jupyterhub provided Django?
c.GenericOAuthenticator.client_id = os.environ['OAUTH2_CLIENT_ID']
c.GenericOAuthenticator.client_secret = os.environ['OAUTH2_CLIENT_SECRET']

# Where can Jupyterhub get the token from?
c.GenericOAuthenticator.token_url = os.environ['OAUTH2_TOKEN_URL']

# Where can it get the user name from? What method shall it use?
# What key in the JSON output is the username?
c.GenericOAuthenticator.userdata_url = os.environ['OAUTH2_USERDATA_URL']
c.GenericOAuthenticator.userdata_method = 'GET'
c.GenericOAuthenticator.userdata_params = {}
c.GenericOAuthenticator.username_key = 'username'

# What address will Jupyterhub be accessed from?
c.JupyterHub.bind_url = os.environ['JUPYTERHUB_URL']

c.JupyterHub.tornado_settings = {
    'headers': {
        'Content-Security-Policy': 'frame-ancestors self *',
    }
}

c.Spawner.args = ['--config=./jupyterhub_notebook_config.py']

# By default Jupyterhub requires that a Linux user exist for every
# authenticated user. For testing, we are going to trick JupyterHub
# to merely pretend that such a user exists and launch notebook servers
# for the same user running the hub process itself!
# from jupyterhub.spawner import LocalProcessSpawner

# class SameUserSpawner(LocalProcessSpawner):
#     """Local spawner that runs single-user servers as the same user as the Hub itself.

#     Overrides user-specific env setup with no-ops.
#     """

#     def make_preexec_fn(self, name):
#         """no-op to avoid setuid"""
#         return lambda : None

#     def user_env(self, env):
#         """no-op to avoid setting HOME dir, etc.""" 
#         return env

# c.JupyterHub.spawner_class = SameUserSpawner
# Spawn single-user servers as Docker containers
c.JupyterHub.spawner_class = 'dockerspawner.DockerSpawner'

# Spawn containers from this image
c.DockerSpawner.image = os.environ['DOCKER_NOTEBOOK_IMAGE']

# JupyterHub requires a single-user instance of the Notebook server, so we
# default to using the `start-singleuser.sh` script included in the
# jupyter/docker-stacks *-notebook images as the Docker run command when
# spawning containers.  Optionally, you can override the Docker run command
# using the DOCKER_SPAWN_CMD environment variable.
spawn_cmd = os.environ.get('DOCKER_SPAWN_CMD', "start-singleuser.sh")
c.DockerSpawner.extra_create_kwargs.update({'command': spawn_cmd})

# Connect containers to this Docker network
network_name = os.environ['DOCKER_NETWORK_NAME']
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.network_name = network_name

# Pass the network name as argument to spawned containers
c.DockerSpawner.extra_host_config = {
    'network_mode': network_name
}

# Explicitly set notebook directory because we'll be mounting a host volume to
# it.  Most jupyter/docker-stacks *-notebook images run the Notebook server as
# user `jovyan`, and set the notebook directory to `/home/jovyan/work`.
# We follow the same convention.
notebook_dir = os.environ.get('DOCKER_NOTEBOOK_DIR') or '/home/jovyan/work'
c.DockerSpawner.notebook_dir = notebook_dir

# Mount the real user's Docker volume on the host to the notebook user's
# notebook directory in the container
AWS_BUCKET_PREFIX = os.environ.get("AWS_BUCKET_PREFIX")
c.DockerSpawner.volumes = {
    'doganai-v1-{username}': notebook_dir
}
c.DockerSpawner.volume_driver = 'rexray/s3fs'

# Remove containers once they are stopped
c.DockerSpawner.remove_containers = True

# For debugging arguments passed to spawned containers
c.DockerSpawner.debug = True

# User containers will access hub by container name on the Docker network
c.JupyterHub.hub_ip = 'jupyterhub'
c.JupyterHub.hub_port = 8001
c.JupyterHub.allow_named_servers = True

c.SlurmSpawner.start_timeout = 12000
c.SlurmSpawner.http_timeout = 12000
