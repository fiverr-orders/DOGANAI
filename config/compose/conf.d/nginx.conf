upstream jupyterhub_server {
    server jupyterhub:8000;
}

upstream django_server {
    server django:7000;
}

client_max_body_size 1024M;

server {
    listen 80;

    location /jupyter {
        proxy_pass http://jupyterhub_server;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;
        add_header Access-Control-Allow-Origin *;
        add_header Content-Security-Policy "frame-ancestors 'self' *";
        add_header X-Frame-Options "ALLOW-FROM *";
    }

    location ~ /api/kernels/ {
        proxy_pass            http://jupyterhub_server;
        proxy_set_header      Host $host;
        # websocket support
        proxy_http_version    1.1;
        proxy_set_header      Upgrade "websocket";
        proxy_set_header      Connection "Upgrade";
        proxy_read_timeout    86400;
    }

    location ~ /terminals/ {
        proxy_pass            http://jupyterhub_server;
        proxy_set_header      Host $host;
        # websocket support
        proxy_http_version    1.1;
        proxy_set_header      Upgrade "websocket";
        proxy_set_header      Connection "Upgrade";
        proxy_read_timeout    86400;
    }

    location / {
        proxy_pass http://django_server;

        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_redirect off;
        add_header Access-Control-Allow-Origin *;
        add_header Content-Security-Policy "frame-ancestors 'self' *";
        add_header X-Frame-Options "ALLOW-FROM *";
    }
}
