# Installation
This project is designed to work with aws. It uses EC2 and S3 services from the aws stack.

## Pre-requisites
### System Setup -  EC2
Make sure an EC2 instance is already running and ssh access is working. Then, perform the following

- ssh into the EC2 instance
- install docker desktop edition
- install docker-compose
- make sure EC2 instance has 25GB Free space available to work with spawned containers

Once the above is done, the next step is to setup a user account in AWS

### Setup user account in AWS for S3 Access
In the AWS Console add a new user and make sure to generate a set of Access Keys. You will be prompted to download the keys and you should do so. We're going to limit the access to just S3.

#### Adding the user
- Log into the console, select the Services and choose IAM...

![IAM Dashboard](/docs/images/1-iam-dashboard.png)

- Next, select the "Add user" button. Type in a username and check the "Programmatic Access" checkbox.  This user does not need the "AWS Management Console access" and you may leave it unchecked. Click "Next".

![Add user box](/docs/images/2-add-user-box.png)

- Now select the "Attach existing policies directly", and click the checkbox next to the policy "AmazonS3FullAccess" (Don't click on the name, as this jumps you over to the policy detail screen). Click "Next". 

![Permissions list](/docs/images/3-permissions-list.png)

- You're now on the review screen.  If everything looks fine click "Create User".  This will generate the API keys which you should download using the "Download .csv" link.  This file is a simple text file which holds the user and secret access keys, and is used to configure the plugin. 

![Download link](/docs/images/4-download-link.png)

### Install RayRax Plugin

#### Introduction
RexRay is a plugin module available for use with Docker which provides the ability to use shared storage as a Docker volume. It is quick to setup and provides near seamless data sharing between containers.

The plugin design supports many different environments. We are going to work with s3 environment only in rexray distribution

#### Installation
Copy the S3 `access_key` and `secret_key` from the file downloaded in the previous step and replace it in the following command

    sudo docker plugin install rexray/s3fs \
        S3FS_ACCESSKEY=<your_access_key> \
        S3FS_SECRETKEY=<your_secret_key> \
        S3FS_REGION=us-west-1 \
        S3FS_OPTIONS="allow_other,nonempty,use_path_request_style,url=https://s3-us-west-1.amazonaws.com" --grant-all-permissions
    
Once loaded you will be able to see the plugin, as well as your S3 buckets, represented as Docker volumes.

    Docker plugin ls
    Docker volume ls
    
## Running the Project
When you are done with the pre-reqs, do the following to run the project

- ssh into your ec2 instance
- pull the scipy-notebook docker image `docker pull jupyter/scipy-notebook`
- clone this repo `git clone https://github.com/doganai/DOGANAI.git`    
- `cd` to `DOGANAI` directory
- checkout falcon branch `git checkout falcon`
- run `docker-compose up` or `docker-compose up -d` to run it in the background
- open the browser and type your ec2 instance IP followed by port `8080` and confirm the project is running

## Re-running the project
If the project is already running and you want to run the update code, do the following

- ssh into your ec2 instance
- `cd` into your project directory
- make sure you are on the `falcon` branch
- run `docker-compose down` to stop all the running containers
- run `git pull` to get the latest code
- run `docker-compose up` to bring them up again with new code
 
## OAuth Setup for Jupyter
The project has integrated oauth setup with jupyter and django website. After running the website you have to perform one additional step to make it work

Assuming your project is running on `ec2-18-204-202-142.compute-1.amazonaws.com:8080`. Do the following

- open `ec2-18-204-202-142.compute-1.amazonaws.com:8080/o/applications` (you have to be logged in to perform this action)
- Then click "Click here" and enter the following information.
    - Name: `JupyterHub`
    - Client id: (can leave as is)
    - Client secret: (can leave as is)
    - Client type: `Confidential`
    - Authorization grant type: `Authorization code`
    - Redirect urls: `http://ec2-18-204-202-142.compute-1.amazonaws.com:8080/jupyter/hub/oauth_callback`
- Click Save

## Environment Variables & Settings
The application expects the following environment variables set in the `.env` file in the root of the project

### Postgres
    POSTGRES_HOST=<container_name_for_postgres>
    POSTGRES_PORT=5432
    POSTGRES_DB=<db_name>
    POSTGRES_USER=<db_user>
    POSTGRES_PASSWORD=<db_password>

### Docker
    DOCKER_NETWORK_NAME=jupyterhub_network
    DOCKER_NOTEBOOK_IMAGE=jupyter/scipy-notebook
    DOCKER_NOTEBOOK_DIR=/home/jovyan/work
    DOCKER_SPAWN_CMD=start-singleuser.sh --SingleUserNotebookApp.default_url=/lab --allow-root --debug

### OAuth2
    OAUTH2_AUTHORIZE_URL=http://<ec2_host_url>:8080/o/authorize
    OAUTH2_TOKEN_URL=http://<ec2_host_url>:8080/o/token/
    OAUTH2_CALLBACK_URL=http://<ec2_host_url>:8080/jupyter/hub/oauth_callback
    OAUTH2_USERDATA_URL=http://<ec2_host_url>:8080/userdata

### Application URLs
    JUPYTERHUB_URL=http://:8000/jupyter
    DOGANAI_URL=http://<ec2_host_url>:8080
    
### Django
    DJANGO_READ_DOT_ENV_FILE=True
    DATABASE_URL=<postgresql_or_sqlite_db_url>

### AWS
    AWS_ACCESS_KEY_ID=<access_key>
    AWS_SECRET_ACCESS_KEY=<secret_key>
    AWS_LOCATION=/data
    AWS_DEFAULT_REGION=us-west-1
    AWS_BUCKET_PREFIX=doganai-v1
    
### Paypal
    PAYPAL_RECEIVER_EMAIL=<merchent_email_from_paypal_dashbaord>
    PAYPAL_TEST=True
    
Set `PAYPAL_TEST` to `False` and `PAYPAL_RECEIVER_EMAIL` to real merchant email for production